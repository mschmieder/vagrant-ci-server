#!/usr/bin/env bash
set -e

echo "Bootstrap: installing puppet..."

if [ "$EUID" -ne "0" ] ; then
        echo "Script must be run as root." >&2
        exit 1
fi

if which puppet > /dev/null ; then
        echo -e "\t puppet is already installed. exiting..."
else
  echo -e "\t running apt-get update..."
  apt-get update -qq

  echo -e "\t installing wget..."
  apt-get install -y -q wget

  system_codename=$(lsb_release -c | grep -Eo "(\w+)$")
  puppet_filename="puppetlabs-release-pc1-${system_codename}.deb"
  puppet_download_file="https://apt.puppetlabs.com/${puppet_filename}"

  echo -e "\t downloading ${puppet_download_file} to /tmp/"
  wget --quiet ${puppet_download_file} -P /tmp/

  echo -e "\t installing puppet package resource..."
  dpkg -i /tmp/${puppet_filename}

  echo -e "\t deleting install package..."
  rm -f /tmp/${puppet_filename}

  # update apt-get to get puppet-agent packages
  apt-get update -qq

  echo -e "\t running apt-get update..."
  apt-get update -qq

  echo -e "\t installing puppet..."
  apt-get install -y -q puppet-agent

  echo -e "\t adding puppet binaries to system path..."

  set +e
  IS_IN_PATH=$(grep -E "export\s+PATH=.*/opt/puppetlabs/bin" /etc/profile)
  set -e
  if [ -z "${IS_IN_PATH}" ];then
    echo "export PATH=$PATH:/opt/puppetlabs/bin" >> /etc/profile
  fi

  echo -e "\t successfully installed puppet. You can now start provisioning."
fi

#################################################################
# ADD SPECIAL PARTITION FOR JENKINS
#################################################################

# if [ -d "/opt/jenkins" ]; then
#   echo -e "additional disk already mounted"
# else
#   echo -e "installing parted"
#   apt-get install -y -q parted xfsprogs

#   echo -e "create partion tables"
#   parted /dev/sdb mklabel msdos
#   parted /dev/sdb mkpart primary 10GiB 100%
#   mkfs.xfs /dev/sdb1

#   echo -e "mounting disk to /opt/jenkins"
#   mkdir -p /opt/jenkins
#   echo `blkid /dev/sdb1 | awk '{print$2}' | sed -e 's/"//g'` /opt/jenkins   xfs   noatime,nobarrier   0   0 >> /etc/fstab
#   mount /opt/jenkins
# fi



echo "Bootstrap: linking hiera files..."
rm -f /etc/puppetlabs/code/hiera.yaml
ln -s /vagrant/puppet/hiera/hiera.yaml /etc/puppetlabs/code/hiera.yaml

rm -rf /etc/puppetlabs/code/hieradata
ln -s /vagrant/puppet/hiera/hieradata/ /etc/puppetlabs/code/hieradata
echo -e "\t finished"

exit 0
