#Jenkins Continuous Environment

[TOC]

This project enables you to "declaratively" configure your continuous integration environment. 
It makes use of the provisioning software 'puppet' as well as 'vagrant' for rapid system setups

## Prequisites
To run this project and create your own conitnuous integration environment you'll need

   1. vagrant
   2. puppet 
   3. VirtualBox
   4. git

### Install VirtualBox
Make sure you install the newest version (>5.0) of VirtualBox.
You can download VirtualBox from https://www.virtualbox.org or install it via your package manager

### Install Vagrant
Make sure you have the newest version installed. Go to https://www.vagrantup.com/downloads.html and download the latest
package corresponding to your system.

  1. **Ubuntu** 

    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.deb
    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_i686.deb

  2. **MacOS**
    
    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1.dmg

  3. **Windows**

    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1.msi

  4. **Centos**
    
    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_x86_64.rpm
    - https://releases.hashicorp.com/vagrant/1.8.1/vagrant_1.8.1_i686.rpm 

### Install Puppet
In order to get your puppet modules straight we need you to install the **puppent-agent** on your host
system. An detailed installation description can be found here https://docs.puppetlabs.com/puppet/4.3/reference/install_linux.html

In order to get you going fast, we prepared some installation routines for your

#### Installation on Ubunu/Mint
Copy this into your terminal

```bash
sudo apt-get update -qq
sudo apt-get install -y -q wget
wget --quiet https://apt.puppetlabs.com/puppetlabs-release-pc1-$(lsb_release -c | grep -Eo "(\w+)$").deb" -P /tmp/
sudo dpkg -i /tmp/puppetlabs-release-pc1-$(lsb_release -c | grep -Eo "(\w+)$").deb
rm -f /tmp/${puppet_filename}
sudo apt-get update -qq
sudo apt-get install -y -q puppet-agent
sudo echo "export PATH=$PATH:/opt/puppetlabs/bin" >> /etc/profile
```

#### Installation on MacOS/Windows
Just download the latest installer and run it

   1. **MacOS/El Capitan**
    
    - https://downloads.puppetlabs.com/mac/10.11/PC1/x86_64/

   2. **MacOS/Yosemite**
    
    - https://downloads.puppetlabs.com/mac/10.10/PC1/x86_64/

   3. **Windows**
   
    - https://downloads.puppetlabs.com/windows/puppet-agent-x64-latest.msi
    - https://downloads.puppetlabs.com/windows/puppet-agent-x86-latest.msi


### Install Git
Use your package system to install git or go to https://git-scm.com and download the latest version of the 
version control system

## Setup the Environment
Checkout the current version of the Project from git@bitbucket.org:mschmieder/vagrant-ci-server.git

```bash
git clone git@bitbucket.org:mschmieder/vagrant-ci-server.git
```

Change into the directory and use the provided setup-script to install missing modules. 
This will checkout missing puppet modules as well as prepare your vagrant installation to support virtualbox machines

```bash
cd vagrant-ci-server
./scripts/bash/setup.sh
```

## Startup the Continuous Integration Environment
To startup the environment just type 

```bash
vagrant up
```

This will start the download of the vagrant base box and will then provision the server with all necessary software.
The installation process may take some time depending on your internet connection as well as your host ancmachine's performance. 

When finished open your favorite browser and got to http://192.168.250.10 
Here you'll find a running Jenkins instance with a simple job example that was provisioned by the setup process.

## Customization


## Jenkins Job Definition
This project makes use of https://bitbucket.org/mschmieder/puppet-jenkins_job so it is possible to 
define your custom jenkins jobs from your puppet manifests. In Order to create a jenkins job using these interfaces
you'll only have to add the following few lines to your manifest code.

```ruby
jenkins_job::create { 'job' :
   scm => 
   [
      {
       'type' => 'GitSCM',
       'params' =>  {
          'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job',
          'branch' => 'master',
          'extensions' => {
              'type' => 'RelativeTargetDirectory',
              'params' => { 'relativeTargetDir' => 'jenkins_job'}
              }
          },
      }
   ],
}
```

The **puppet-jenkins_job** module supports a variety of jenkins configurations but cannot give you full fledged support for **all** configuration types there are. The supported configuration types are listet below.

### Managing Matrix Jobs
The jenkins_job module has build in support for matrix build job. To create a simple matrix build job just add the **axes** parameter to your resource definition, this will automatically switch the xml generation to a matrix job definition

As you are surely familier with, there are three different types of matrix axis that can be use

  - **TextAxis**: Just adds a new axis with **name** and **values**
  - **LabelExpAxis*
  *: Here you can define label expressions that will be evaluated when choosing a slave on which the job will be executed
  - **LabelAxis**: Almost the same as LabelExpAxis but here you're only allowed to select labels and cannot define label expression

A simple matrix job definition would therefore look like this:

```ruby
jenkins_job::create { 'matrix-job' :
  axes => [
    {
      'type' => 'TextAxis', 
      'params' => {
        'name' => "Compiler",
        'values' => ['gcc-4.9','gcc-5.2','clang-3.7']
      }
    },    
    {
      'type' => 'LabelAxis', 
      'params' => {
        'name' => "System",
        'values' => ['linux']
      }
    },
  ],
  builders => [
      {
         'type' => 'Shell',
         'params' =>  {
            'command' => 'echo "Command is executed on ${System} compiling with ${Compiler}"'
         }
      }
   ]
}
```

### Jenkins Configuration modules
Jenkins modules are divided into categories
   
   1. Properties
   2. Triggers
   3. Builders
   4. Publishers
   5. BuildWrappers 

#### Properties

##### BuildDiscardProperty
Defines when build artifacts are considered discardable

**defaults**:
```json
{
   "strategy": "LogRotator",
   "daysToKeep": "-1",
   "numToKeep": "-1",
   "artifactDaysToKeep": "-1",
   "artifactNumToKeep": "-1"
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
 properties => 
 [
   { 
     'type' => 'BuildDiscarderProperty',
     'params' => { "numToKeep" => "2" }                
   }
 ],
}
```


#### Triggers

##### SCMTrigger
this trigger checks the source code menagement for changes

**defaults**:
```json
{
  "spec": "",
  "ignorePostCommitHooks":"false"
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   triggers => 
   [ 
     {
       'type' => "SCMTrigger",
       'params' => {'spec' => 'H/10 * * * *'}
     },
   ],
}
```

##### TimerTrigger
triggers build after a given timespan/timepoint

**defaults**:
```json
{
  "spec": "",
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   triggers => 
   [ 
     {
       'type' => "TimerTrigger",
       'params' => {'spec' => 'H/10 * * * *'}
     },
   ],
}
```

##### ReverseBuildTrigger
triggers build after/before an other project is build

**defaults**:
```json
{
  "spec":"",
  "upstreamProjects":"",
  "threshold": {
    "name":"SUCCESS",
    "ordinal":"0",
    "color":"BLUE",
    "completeBuild":"true"
  }
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   triggers => 
   [ 
     {
       'type' => "ReverseBuildTrigger",
       'params' => 
       {
         'spec' => 'H/10 * * * *',
         'upstreamProjects' => 'upstream_project'
       }
     },
   ],
}
```

#### SCM
This part of the jenkins job configuration defines how the source code management of the job is build up

##### GitSCM (jenkins git plugin)
This will use the git plugin to checkout any given source from a defined location

**ATTENTION**:
Jenkins git plugin support is limited to the given parameter set. More complicated plugin configurations are not
supported (yet) and my yield a change in parameter definitions in future

**defaults**:

**Usage**:
```ruby
jenkins_job::create { 'job' :
   scm => 
   [
       {
        'type' => 'GitSCM',
        'params' =>  {
           'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job.git',
           'branch' => 'master',
           'extensions' => {
               'type' => 'RelativeTargetDirectory',
               'params' => { 'relativeTargetDir' => 'license'}
               }
           },
       }
   ],
}
```

#### Builders

#### Shell
Simple shell command builder

```ruby
jenkins_job::create { 'job' :
   builders => 
   [
      {
         'type' => 'Shell',
         'params' =>  {
            'command' => 'echo "running shell command"'
         }
      }
   ],
}
```

#### Batch
Simple batch command builder

```ruby
jenkins_job::create { 'job' :
   builders => 
   [
      {
         'type' => 'Batch',
         'params' =>  {
            'command' => 'echo "running batch command"'
         }
      }
   ],
}
```

#### CopyArtifact
Copies artifacts from another project

**defaults**:
```json
{
    "doNotFingerprintArtifacts": false,
    "excludes": "",
    "filter": "",
    "parameters": "",
    "project": "",
    "resultVariableSuffix": "",
    "selector": {
        "type" : "StatusBuildSelector",
        "params" : {
          "stable": true
        } 
      },
    "target": ""
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   builders => 
   [
      {
         'type' => 'CopyArtifact',
         'params' =>  {
            'project' => "ProjectToCopyFrom",
            'excludes' => "*exluded_artifacts.tar",
            'filter' => "*included_artifacts.tar", 
            'selector' => {
               'type' => "StatusBuildSelector",
               'params' => { 'stable' => "true" }
            }
         }
      }
   ],
}
```

#### TriggerBuilder
Triggers Build on other job

**defaults**:
```json
{
  "buildAllNodesWithLabel": "false",
  "condition": "ALWAYS",
  "projects": "",
  "triggerWithNoParameters": "false",
  "block": "false",
  "configs" : {}
}
```


**configs**

there are several different **configs** that can be used to influence the triggered build jobs.
make sure you setup the **configs** parameter accordingly

   - FileBuildParameters
```ruby
'configs' => {
         'FileBuildParameters' => {
         'propertiesFile' => 'propfile.txt',
         'encoding' => '',
         'failTriggerOnMissing' => 'true',
         'useMatrixChild' => 'false',
         'onlyExactRuns' => 'false'
       }
}
```

   - BooleanParameters
```ruby
'configs' => {
      'BooleanParameters' => {
         'configs' => [
            {
              'BooleanParameterConfig' => {
                "name" => 'firstbool',
                "value" => "false" }
            },
            {
              'BooleanParameterConfig' => {
                "name" => 'secondbool',
                "value" => "false" }
            }
         ]
      }
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   builders => 
   [
      {
         'type' => 'TriggerBuilder',
         'params' => 
         {
            'triggers' => [ 
              {           
              'projects' => 'my_next_project1',
              'condition' => 'ALWAYS',
              'triggerWithNoParameters' => "false",
              'buildAllNodesWithLabel' => "false", 
              'block' => "false",
              'configs' => 
              {
                 'FileBuildParameters' => 
                 {
                    'propertiesFile' => 'propfile.txt',
                    'encoding' => '',
                    'failTriggerOnMissing' => 'true',
                    'useMatrixChild' => 'false',
                    'onlyExactRuns' => 'false'
                 },
                 'BooleanParameters' => 
                 {
                    'configs' => 
                    [
                       {
                         'BooleanParameterConfig' => 
                          {
                             "name" => 'firstbool',
                             "value" => "false" 
                          }
                       },
                       {
                          'BooleanParameterConfig' => 
                          {
                             "name" => 'secondbool',
                             "value" => "false" 
                          }
                       }
                    ]
                 }
              }
            }
           ]
         }
      }
   ],
}
```

#### Publishers

##### WsCleanup
This is used to cleanup the workspace after the project was build
**Usage**:
```ruby
jenkins_job::create { 'job' :
   build_wrappers => 
   [
      {
         'type' => 'WsCleanup',
         'params' => {
            "patterns" =>
            [ 
              {
                "pattern" => "**/*include.tar",
                "type" => "INCLUDE"
              },
              {
                "pattern" => "**/*exlude.tar",
                "type" => "EXCLUDE"
              } 
            ],
            "deleteDirs" => "false",
            "cleanupParameter" => "",
            "externalDelete" => "" 
         }
      }
   ],
}
```

##### ArtifactArchiver
This Plugin is used to archive any given artifacts from your build job

**defaults**:
```json
{
  "artifacts": "",
  "allowEmptyArchive": "false",
  "onlyIfSuccessful": "false",
  "fingerprint": "false",
  "defaultExcludes": "true",
  "caseSensitive": "true"
}
```

**Usage**:
```ruby
jenkins_job::create { 'job' :
   publishers => 
   [
      {
         'type' => 'ArtifactArchiver',
         'params' =>  {
            "artifacts" => "**/*.tar.gz",
         }
      }
   ],
}
```

#### BuildTrigger
this is actually the same configuration as in Builders::TriggerBuilder. Just change type to 'BuildTrigger'

**Usage**:
```ruby
jenkins_job::create { 'job' :
   publishers => 
   [
      {
         'type' => 'BuildTrigger',
         'params' => 
         { 
           'triggers' => 
           [
             {
                'projects' => 'my_next_project1',
                'condition' => 'ALWAYS',
                'triggerWithNoParameters' => "false",
                'buildAllNodesWithLabel' => "false", 
                'block' => "false",
                'configs' => 
                {
                   'FileBuildParameters' => 
                   {
                      'propertiesFile' => 'propfile.txt',
                      'encoding' => '',
                      'failTriggerOnMissing' => 'true',
                      'useMatrixChild' => 'false',
                      'onlyExactRuns' => 'false'
                   },
                }
            }
          ]
        }
      }
   ],
}
```


##### JoinTrigger
see **Publishers::BuildTrigger** on how to configure the joinPublishers

**Usage**:
```ruby
jenkins_job::create { 'trigger-builder' :
  publishers => 
  [
    {
      'type' => 'JoinTrigger',
      'params' => 
      {
        'joinPublishers' => 
        [
          {
            'type' => "BuildTrigger",
            'params' =>
            {
              'triggers' => 
              [
                {
                  'projects' => 'my_next_project1',
                  'condition' => 'ALWAYS',
                  'triggerWithNoParameters' => "false",
                  'buildAllNodesWithLabel' => "false", 
                  'block' => "false",
                  'configs' => 
                  {
                    'BooleanParameters' => 
                    {
                      'configs' =>
                       [
                        {
                          'BooleanParameterConfig' => 
                          {
                            "name" => 'firstbool',
                            "value" => "false"
                          }
                        }
                      ]
                    }
                  }
                }
              ]
            }
          }
        ]
      }
    }
  ]
}
```


##### JoinTrigger

#### BuildWrappers

##### PreBuildCleanup
This is used to cleanup the workspace before checkout and/or building your project
**Usage**:
```ruby
jenkins_job::create { 'job' :
   build_wrappers => 
   [
      {
         'type' => 'PreBuildCleanup',
         'params' => {
            "patterns" =>
            [ 
              {
                "pattern" => "**/*include.tar",
                "type" => "INCLUDE"
              },
              {
                "pattern" => "**/*exlude.tar",
                "type" => "EXCLUDE"
              } 
            ],
            "deleteDirs" => "false",
            "cleanupParameter" => "",
            "externalDelete" => "" 
         }
      }
   ],
}
```



### Using hiera to load pre-defined job templates
Since it can get quite hazzardous to use the low-level apprach above to define your build jobs the **jenkins_job** puppet module allows you to combine custom made job templates with the configuration method shown in the previous chapter. The only thing you'll have to do is provide the templates content to the **template** parameter of jenkins_job::create resource

**manifest.pp**
```ruby
jenkins_job::create{ 'VanillaTemplate' : 
  template => hiera('jenkins::templates::jobs::vanilla'),
}
```

**hiera.yaml**
```yaml
jenkins::templates::jobs::vanilla: <?xml version='1.0' encoding='UTF-8'?>
  <project>
    <description>simple template job</description>
    <keepDependencies>false</keepDependencies>
    <properties/>
    <scm class="hudson.scm.NullSCM"/>
    <canRoam>true</canRoam>
    <disabled>false</disabled>
    <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
    <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
    <triggers/>
    <concurrentBuild>false</concurrentBuild>
    <builders/>
    <publishers/>
    <buildWrappers/>
  </project>
```

This code example creates a trivial job that does not do anything but is showing you how to provide and provision pre-defined jenkins jobs to your puppet client.

#### Creating job templates
The easiest way to create such pre-defines templates is by simply defining your job using the jenkins user interface. To get access to the plain xml description the easiest way is to install the **JobConfigHistory Plugin** which will give you direct access from the user interface by clicking **Job Config History** when having the job you just edited selected.


#### Customizing job templates provided by hiera
**jenkins_job** let's you customize your jenkins job template by providing you access to some variables that you can use from the ERB template by invoking ruby code.
See the following definitions to understand on how you can change the behaviour of your jenkins jobs using the variables provided

**AVAILABLE VARIABLES**
  - **String::job_actions_content**:
  Text variable holding the xml content that will be created if any **action** parameter was provided by the user

  - **String::job_publishers_content**:
  Text variable holding the xml content that will be created if any **publishers** parameter was provided by the user

  - **String::job_triggers_content**:
  Text variable holding the xml content that will be created if any **triggers** parameter was provided by the user
  
  - **String::job_scm_content**:
  Text variable holding the xml content that will be created if any **scm** parameter was provided by the user
  
  - **String::job_builders_content**:
  Text variable holding the xml content that will be created if any **builders** parameter was provided by the user
  
  - **String::job_build_wrappers_content**:
  Text variable holding the xml content that will be created if any **build_wrappers** parameter was provided by the user
  
  - **String::job_properties_content**:
  Text variable holding the xml content that will be created if any **properties** parameter was provided by the user
  
  - **Hash::options**:
  Hash variable holding all parameters. You'll have access to 

```ruby
    options = {
      'template'       => {}
      'properties'     => {}
      'description'    => {}
      'scm'            => {}
      'builders'       => {}
      'publishers'     => {}
      'triggers'       => {}
      'build_wrappers' => {}
      'params'         => {}
      'user_options'   => {}
  }
```

  - **Hash::user_options**:
  This variable gives you direct access to the user_options. These options are entirely defined and provided by the user and have no other effect.

**AVAILABLE FUNCTIONS**
 
  - **get_version(plugin_name)::String**:
  This function is used to dynamically adapt to plugin version changes that will corrupt your xml definitions

Let's say you have defined your jenkins template in a way so that only the source code management part of should be configured by the puppet manifest. This can be done by providing the **jenkins_job::create** resource with the scm parameter and referencing to it form inside the actual template content

**hiera.yaml**
```yaml
jenkins::templates::jobs::customizeable_job: <?xml version='1.0' encoding='UTF-8'?>
  <project>
    <description>simple template job</description>
    <keepDependencies>false</keepDependencies>
    <properties/>
<% if job_scm_content.emtpy? %>
    <scm class="hudson.scm.NullSCM"/>
<%else%>
    <%= job_scm_content %>
<%end%>
    <canRoam>true</canRoam>
    <disabled>false</disabled>
    <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
    <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
    <triggers/>
    <concurrentBuild>false</concurrentBuild>
    <builders/>
    <publishers/>
    <buildWrappers/>
  </project>
```

Note the ruby part of the template which is encasulated in "<% CODE %>"
 
```xml
<% if job_scm_content.emtpy? %>
    <scm class="hudson.scm.NullSCM"/>
<%else%>
    <%= job_scm_content %>
<%end%>
```

When defining **jenkins_job::create** resource with the scm parameter it will insert the generated scm code at that excact position

```ruby
jenkins_job::create{ 'VanillaTemplate' : 
  template => hiera('jenkins::templates::jobs::vanilla'),
  scm => 
  [
      {
       'type' => 'GitSCM',
       'params' =>  {
          'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job',
          'branch' => 'master',
          'extensions' => {
              'type' => 'RelativeTargetDirectory',
              'params' => { 'relativeTargetDir' => 'jenkins_job'}
              }
          },
      }
   ],
}
```

You can even customize this way further by providing the **user_options** parameter that you can define by your own gusto. 
Let's say you wan't to setup the build job description using your custom parameter (even if there is already a description parameter!). You can easily setup the **jenkins_job::create** resource this way

```ruby
jenkins_job::create{ 'VanillaTemplate' : 
  template => hiera('jenkins::templates::jobs::vanilla'),
  scm => 
  [
      {
       'type' => 'GitSCM',
       'params' =>  {
          'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job',
          'branch' => 'master',
          'extensions' => {
              'type' => 'RelativeTargetDirectory',
              'params' => { 'relativeTargetDir' => 'jenkins_job'}
              }
          },
      }
   ],
   user_options => {
      'my_humble_job_description' => 'fancy description of the current job we got here!'
   }
}
```

You can now access the parameter you just defined by modifying your job template xml just a little bit

```xml
<project>
    <description>
<%= user_options['my_humble_job_description'] %>
    </description>
    ...
</project>
```



##### Wrapping plugin versions
Jenkins will add a definitive plugin version to nodes that are provided by plugins. In order to get your template jobs working we need to find out the installed version of the plugin and adapt the template xml according to it. For this the **jenkins_job** puppet modules provides you with a function that is accessible through ruby's ERB template mechnanism

The the following example, where a job has been defined that uses the **WorkspaceCleanupPlugin** inside the **publishers** section.
As you can easily see, jenkins printed the curren **ws-cleanup** plugin version into the xml file.

```xml
<?xml version='1.0' encoding='UTF-8'?>
  <project>
    <description>simple template job</description>
    <keepDependencies>false</keepDependencies>
    <properties/>
    <canRoam>true</canRoam>
    <disabled>false</disabled>
    <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
    <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
    <triggers/>
    <concurrentBuild>false</concurrentBuild>
    <builders/>
    <publishers>
      <hudson.plugins.ws__cleanup.WsCleanup plugin="ws-cleanup@0.28">
        <patterns>
          <hudson.plugins.ws__cleanup.Pattern>
            <pattern>**/*properties.txt</pattern>
            <type>EXCLUDE</type>
          </hudson.plugins.ws__cleanup.Pattern>
        </patterns>
        <deleteDirs>false</deleteDirs>
        <skipWhenFailed>false</skipWhenFailed>
        <cleanWhenSuccess>true</cleanWhenSuccess>
        <cleanWhenUnstable>true</cleanWhenUnstable>
        <cleanWhenFailure>true</cleanWhenFailure>
        <cleanWhenNotBuilt>true</cleanWhenNotBuilt>
        <cleanWhenAborted>true</cleanWhenAborted>
        <notFailBuild>false</notFailBuild>
        <cleanupMatrixParent>false</cleanupMatrixParent>
        <externalDelete></externalDelete>
      </hudson.plugins.ws__cleanup.WsCleanup>
    </publishers>
   <buildWrappers/>
  </project>
```

In order to get this job template working with any other version of the **ws-cleanup** plugin you'll have to slightly modify your xml content and use the provided function **get_version(plugin_name)** in order to adapt to the changing environment. So change the xml as follows

```xml
<publishers>
  <hudson.plugins.ws__cleanup.WsCleanup plugin="ws-cleanup@<%= get_version('ws-cleanup') %>">
    <patterns>
      <hudson.plugins.ws__cleanup.Pattern>
        <pattern>**/*properties.txt</pattern>
        <type>EXCLUDE</type>
      </hudson.plugins.ws__cleanup.Pattern>
    </patterns>
    <deleteDirs>false</deleteDirs>
    <skipWhenFailed>false</skipWhenFailed>
    <cleanWhenSuccess>true</cleanWhenSuccess>
    <cleanWhenUnstable>true</cleanWhenUnstable>
    <cleanWhenFailure>true</cleanWhenFailure>
    <cleanWhenNotBuilt>true</cleanWhenNotBuilt>
    <cleanWhenAborted>true</cleanWhenAborted>
    <notFailBuild>false</notFailBuild>
    <cleanupMatrixParent>false</cleanupMatrixParent>
    <externalDelete></externalDelete>
  </hudson.plugins.ws__cleanup.WsCleanup>
</publishers>
```