class jenkins_env::scm() inherits ::jenkins_job {

  notice("provisioning jenkins scm jobs")

  ####################################################
  # GITSCM
  ####################################################
  jenkins_job::create { 'GitSCM' :
     scm => 
     [
         {
          'type' => 'GitSCM',
          'params' =>  {
             'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job.git',
             'branch' => 'master',
             'extensions' => {
                 'type' => 'RelativeTargetDirectory',
                 'params' => { 'relativeTargetDir' => 'license'}
                 }
             },
         }
     ],
  }

  ####################################################
  # SUBVERSIONSCM
  ####################################################
  jenkins_job::create { 'SubversionSCM' :
     scm => 
     [
        {
         'type' => 'SubversionSCM',
         'params' => {
            'remote' => 'https://svnurl.com',
            'local' => 'svndir',
            'ignoreExternalsOption' => 'true' }
        }
     ],
  }



  # ####################################################
  # # GITSCM
  # ####################################################
  jenkins_job::create { 'MultiSCM' :
     scm => 
     [
        {
          'type' => 'GitSCM',
          'params' =>  {
             'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job.git',
             'branch' => 'master',
             'extensions' => {
                 'type' => 'RelativeTargetDirectory',
                 'params' => { 'relativeTargetDir' => 'license'}
                 }
             },
        },
        {
          'type' => 'SubversionSCM',
          'params' => {
             'remote' => 'https://svnurl.com',
             'local' => 'svndir',
             'ignoreExternalsOption' => 'true' 
           }
        }
     ],
  }
}




