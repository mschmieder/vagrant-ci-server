class jenkins_env::build_wrappers() inherits ::jenkins_job {

  notice("provisioning jenkins build_wrappers jobs")

  ####################################################
  # BUILDDISCARDERPROPERTY
  ####################################################
  jenkins_job::create { 'Properties_PreBuildCleanup' :
   build_wrappers => 
   [
      {
          'type' => "PreBuildCleanup",
          'params' => {
            "patterns"  =>
            [ 
              {
                "pattern" => "**/*include.tar",
                "type" => "INCLUDE"
              },
              {
                "pattern" => "**/*exlude.tar",
                "type" => "EXCLUDE"
              } 
            ],
            "deleteDirs" => "false",
            "cleanupParameter" => "",
            "externalDelete" => "" 
         }
      },
   ],
  }
}




