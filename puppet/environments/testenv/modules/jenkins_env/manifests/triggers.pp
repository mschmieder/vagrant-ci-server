class jenkins_env::triggers() inherits ::jenkins_job {

  notice("provisioning jenkins trigger jobs")

  ####################################################
  # SCMTRIGGER
  ####################################################
  jenkins_job::create { 'Triggers_SCMTrigger' :
     triggers => 
     [ 
       {
         'type' => "SCMTrigger",
         'params' => {'spec' => 'H/10 * * * *'}
       },
     ],
  }
  ####################################################
  # TIMERTRIGGER
  ####################################################
  jenkins_job::create { 'Triggers_TimerTrigger' :
     triggers => 
     [ 
       {
         'type' => "TimerTrigger",
         'params' => {'spec' => 'H/10 * * * *'}
       },
     ],
  }

  ####################################################
  # REVERSEBUILDTRIGGER
  ####################################################
  jenkins_job::create { 'Triggers_ReverseBuildTrigger' :
     triggers => 
     [ 
       {
         'type' => "ReverseBuildTrigger",
         'params' => 
         {
           'spec' => 'H/10 * * * *',
           'upstreamProjects' => 'upstream_project'
         }
       },
     ],
  }
}