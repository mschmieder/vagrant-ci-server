class jenkins_env::builders() inherits ::jenkins_job {

  notice("provisioning jenkins pro jobs")

  ####################################################
  # COPYARTIFACT
  ####################################################
  jenkins_job::create { 'Builders_CopyArtifact' :
     builders => 
     [
        {
           'type' => 'CopyArtifact',
           'params' =>  {
              'project' => "ProjectToCopyFrom",
              'excludes' => "*exluded_artifacts.tar",
              'filter' => "*included_artifacts.tar", 
              'selector' => {
                 'type' => "StatusBuildSelector",
                 'params' => { 'stable' => "true" }
              }
           }
        }
     ],
  }

  ####################################################
  # TRIGGERBUILDER
  ####################################################
  jenkins_job::create { 'Builders_TriggerBuilder' :
   builders => 
   [
      {
         'type' => 'TriggerBuilder',
         'params' => 
         {
            'triggers' => [ 
              {           
              'projects' => 'my_next_project1',
              'condition' => 'ALWAYS',
              'triggerWithNoParameters' => "false",
              'buildAllNodesWithLabel' => "false", 
              'block' => "false",
              'configs' => 
              {
                 'FileBuildParameters' => 
                 {
                    'propertiesFile' => 'propfile.txt',
                    'encoding' => '',
                    'failTriggerOnMissing' => 'true',
                    'useMatrixChild' => 'false',
                    'onlyExactRuns' => 'false'
                 },
                 'BooleanParameters' => 
                 {
                    'configs' => 
                    [
                       {
                         'BooleanParameterConfig' => 
                          {
                             "name" => 'firstbool',
                             "value" => "false" 
                          }
                       },
                       {
                          'BooleanParameterConfig' => 
                          {
                             "name" => 'secondbool',
                             "value" => "false" 
                          }
                       }
                    ]
                 }
              }
            }
           ]
         }
      }
   ],
  }
}




