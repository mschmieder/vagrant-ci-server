class jenkins_env::properties() inherits ::jenkins_job {

  notice("provisioning jenkins properties jobs")

  ####################################################
  # BUILDDISCARDERPROPERTY
  ####################################################
  jenkins_job::create { 'Properties_BuildDiscarderProperty' :
   properties => 
   [
     { 
       'type' => 'BuildDiscarderProperty',
       'params' => { "numToKeep" => "2" }                
     }
   ],
  }
}




