class jenkins_env::publishers ( ) inherits ::jenkins_env

{

  notice("provisioning jenkins publisher jobs")


  #####################################################
  # ARTIFACTARCHIVER
  #####################################################
  jenkins_job::create { 'Publishers_ArtifactArchiver' :
     publishers => 
     [
        {
           'type' => 'ArtifactArchiver',
           'params' =>  {
              "artifacts" => "**/*.tar.gz",
           }
        }
     ],
  }
  
  #####################################################
  # BUILDTRIGGER
  #####################################################

  jenkins_job::create { 'Publishers_BuildTrigger' :
     publishers => 
     [
        {
           'type' => 'BuildTrigger',
           'params' => 
           { 
             'triggers' => 
             [
               {
                  'projects' => 'my_next_project1',
                  'condition' => 'ALWAYS',
                  'triggerWithNoParameters' => "false",
                  'block' => "false",
                  'configs' => 
                  {
                     'FileBuildParameters' => 
                     {
                        'propertiesFile' => 'propfile.txt',
                        'encoding' => '',
                        'failTriggerOnMissing' => 'true',
                        'useMatrixChild' => 'false',
                        'onlyExactRuns' => 'false'
                     },
                  }
              }
            ]
          }
        }
     ],
  }
  
  #####################################################
  # JOINTRIGGER
  #####################################################

  jenkins_job::create { 'Publishers_JoinTrigger' :
    publishers => 
    [
      {
        'type' => 'JoinTrigger',
        'params' => 
        {
          'joinPublishers' => 
          [
            {
              'type' => "BuildTrigger",
              'params' =>
              {
                'triggers' => 
                [
                  {
                    'projects' => 'my_next_project1',
                    'condition' => 'ALWAYS',
                    'triggerWithNoParameters' => "false",
                    'block' => "false",
                    'configs' => 
                    {
                      'BooleanParameters' => 
                      {
                        'configs' =>
                         [
                          {
                            'BooleanParameterConfig' => 
                            {
                              "name" => 'firstbool',
                              "value" => "false"
                            }
                          }
                        ]
                      }
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    ]
  }

 jenkins_job::create { 'Publishers_WsCleanup' :
    publishers => [
      {
        'type' => 'WsCleanup',
        'params' => {
            "patterns" =>
            [ 
              {
                "pattern" => "**/*include.tar",
                "type" => "INCLUDE"
              },
              {
                "pattern" => "**/*exlude.tar",
                "type" => "EXCLUDE"
              } 
            ],
            "deleteDirs" => "false",
            "cleanupParameter" => "",
            "externalDelete" => "" 
         }
      },
    ]
  }
}

jenkins_job::create { 'Publishers_ClangScanBuildPublisher' :
    publishers => [
      {
        'type' => 'ClangScanBuildPublisher',
        'params' => {
          "reportFolderName" => "metrics",
        }
      },
    ]
  }
}

jenkins_job::create { 'Publishers_CppcheckPublisher' :
    publishers => 
    [
      {
        'type' => 'CppcheckPublisher',
        'params' => {
          "cppcheckConfig" => {
            "pattern"          => "cppeckxml.xml",
            "ignoreBlankFiles" => "true",
            "allowNoReport"    => "true",
            "configSeverityEvaluation" => {
               "threshold"           => "30",
               "newThreshold"        => "40",
               "failureThreshold"    => "50",
               "newFailureThreshold" => "60",
               "healthy"             => "10",
               "unHealthy"           => "20",
               "severityError"       => "true",
               "severityWarning"     => "true",
               "severityStyle"       => "true",
               "severityPerformance" => "true",
               "severityInformation" => "true",
               "severityNoCategory"  => "true",
               "severityPortability" => "true",
            },
            "configGraph" => {
                "xSize"                      => "500",
                "ySize"                      => "200",
                "numBuildsInGraph"           => "0",
                "displayAllErrors"           => "true",
                "displayErrorSeverity"       => "true",
                "displayWarningSeverity"     => "true",
                "displayStyleSeverity"       => "true",
                "displayPerformanceSeverity" => "true",
                "displayInformationSeverity" => "true",
                "displayNoCategorySeverity"  => "true",
                "displayPortabilitySeverity" => "true",
            }
          }
        }
      }
    ]
  }
}