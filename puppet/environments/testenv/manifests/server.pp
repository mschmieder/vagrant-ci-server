Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

file { '/home/vagrant/.ssh/id_rsa' :
  source => 'puppet:///data/id_rsa_jenkins',
  ensure => present,
  owner  => 'vagrant',
  group  => 'vagrant',
  mode   => '700'
}

################################################################################
# SETUP PACKAGING SYSTEM WITH APT
################################################################################
class { 'apt':
update => {
  frequency => 'daily',
  },
}

################################################################################
# INSTALL BASE SYSTEM
################################################################################
$sysPackages = [ 'wget', 'nano', 'pkg-config']
package { $sysPackages:
  ensure => 'latest',
}

################################################################################
# GIT SETUP
################################################################################
class { 'git': }


git::config { 'user.name':
  value => 'jenkins',
}

git::config { 'user.email':
  value => "jenkins@$fqdn",
}

notice("JENINS-MASTER-NODE: running jenkins server configuration")
################################################################################
# JENKINS BASE INSTALLATION
################################################################################
class { 'jenkins':
  executors => 2,
}
# change jenkins home
$jenkins_home=hiera('jenkins_home')
file_line  { 'patch_jenkins_home' :
  line     => "JENKINS_HOME=$jenkins_home",
  path     => '/etc/default/jenkins',
  ensure   => 'present',
  match    => "JENKINS_HOME=.*",
  require  => Class['jenkins'],
}
exec { 'mkdir_jenkins_ssh' :
  command => "mkdir -p $jenkins_home/.ssh; chown -R jenkins:jenkins $jenkins_home/.ssh",
  require => Class['jenkins']
}
file { "$jenkins_home/.ssh/id_rsa" :
  source => 'puppet:///data/id_rsa_jenkins',
  ensure => present,
  owner  => 'jenkins',
  group  => 'jenkins',
  mode   => '700',
  require => [Class['jenkins'],Exec['mkdir_jenkins_ssh']]
}
jenkins::credentials { 'bitbucket-deploy-key':
  password            => '',
  description         => 'Credentials to access bitbucket',
  private_key_or_path => "$jenkins_home/.ssh/id_rsa",
}
jenkins::user { 'jenkins':
  email     => "jenkins@$fqdn",
  password  => 'jenkins@home',
  full_name => 'Jenkins CI',
}
jenkins::user { 'mschmieder':
  email     => "schmieder.matthias@gmail.com",
  password  => 'jenkins@home',
  full_name => 'Matthias Schmieder',
}
# patch rc.local to port forward 80 to 8080
file_line {'rm_exit0':
  line      => 'exit 0',
  path      => '/etc/rc.local',
  ensure    => 'absent',
}->
file_line {'port_map_from_outside':
  line      => 'iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports 8080',
  path      => '/etc/rc.local',
  ensure    => 'present',
}->
file_line {'port_map_from_inside':
  line      => 'iptables -t nat -I OUTPUT -p tcp -d 127.0.0.1 --dport 80 -j REDIRECT --to-ports 8080',
  path      => '/etc/rc.local',
  ensure   => 'present',
}->
file_line {'insert_exit0':
  line      => 'exit 0',
  path      => '/etc/rc.local',
  ensure    => 'present',
}->exec { 'reload_iptables':
  command      => '/etc/rc.local',
}

# install git plugin
jenkins::plugin { 'git': }
jenkins::plugin { 'git-client': }
jenkins::plugin { 'ssh-credentials': }
jenkins::plugin { 'mailer': }
jenkins::plugin { 'promoted-builds': }
jenkins::plugin { 'matrix-project': }
jenkins::plugin { 'parameterized-trigger': }
jenkins::plugin { 'token-macro': }
jenkins::plugin { 'credentials': }
jenkins::plugin { 'scm-api': }
jenkins::plugin { 'ws-cleanup': }
jenkins::plugin { 'jobConfigHistory': }
jenkins::plugin { 'maven-plugin': }
jenkins::plugin { 'javadoc': }
jenkins::plugin { 'junit': }
jenkins::plugin { 'ansicolor' : }
jenkins::plugin { 'copyartifact': }
jenkins::plugin { 'script-security': }
jenkins::plugin { 'multiple-scms': }
jenkins::plugin { 'jobcopy-builder' : }
jenkins::plugin { 'join' : }
jenkins::plugin { 'downstream-ext' : }
jenkins::plugin { 'swarm' : }


define pip3::package () {
  exec { "$name" :
    command      => "pip3 install $name",
    path         => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    require      => Package['python3-pip']
  }
}


notice("JENKINS-NODE: running jenkins build configuration")

################################################################################
# BUILD ENVIRONMENT SETUP
################################################################################
$buildtools_packages = 
[ 'gcc',
  'gcc-4.9',
  'clang',
  'cmake',
  'python3-pip',
  'libtinyxml2-dev',
  'libjsoncpp-dev']


package { $buildtools_packages:
  ensure => 'latest',
}

class { 'gtest' : }

class { 'opencv' :
        dependencies_only => true,
        manage_packages => true 
      }

pip3::package{ 'pysftp' : }
pip3::package{ 'paramiko' : }
pip3::package{ 'beautifulsoup4' : }

