
class { 'jenkins_env::properties' :
}
class { 'jenkins_env::triggers' :  
}
class { 'jenkins_env::scm' :  
}
class { 'jenkins_env::publishers' :  
}
class { 'jenkins_env::builders' :  
}
class { 'jenkins_env::build_wrappers' :  
}

jenkins_job::create{ 'INIT_GITSCM' : 
  template => hiera('jenkins::templates::jobs::init_job'),
  scm => 
  [
    {
      'type' => "GitSCM",
      'params' =>  {
        'url' => 'git@bitbucket.org:mschmieder/puppet-jenkins_job.git',
        'branch' => 'master',
        'extensions' => {
            'type' => 'RelativeTargetDirectory',
            'params' => { 'relativeTargetDir' => 'license'}
            }
        },
    }
  ]
}
