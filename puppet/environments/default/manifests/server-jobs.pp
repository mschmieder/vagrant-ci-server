# jenkins_job::create{ 'opencv3.0-linux' : 
#   properties => 
#   [
#     { 
#       'type' => 'BuildDiscarderProperty',
#       'params' => { "numToKeep" => "2" }                
#     }
#   ],
#   triggers => 
#   [ 
#     {
#       'type' => "SCMTrigger",
#       'params' => {'spec' => 'H/60 * * * *'}
#     },
#   ],
#   scm => 
#   [
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'https://github.com/Itseez/opencv.git',
#         'branch' => '3.0.0',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'opencv'}
#             }
#         },
#     },
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'git@bitbucket.org:mschmieder/build-tools.git',
#         'branch' => 'master',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'build-tools'}
#             }
#         },
#     }
#   ],
#   builders => 
#   [
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# set -e
# mkdir -p $WORKSPACE/build
# mkdir -p $WORKSPACE/install/opencv

# cd $WORKSPACE/build
# cmake $WORKSPACE/opencv -DCMAKE_BUILD_TYPE=RELEASE -DCMAKE_INSTALL_PREFIX=$WORKSPACE/install/opencv

# make
# make install

# cd $WORKSPACE/install
# tar -zcf $WORKSPACE/opencv3.0.0.tar.gz *
# '
#         },
#     },
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# rm -rf $WORKSPACE/build
# rm -rf $WORKSPACE/install/opencv
# '
#       },
#     }
#   ],
#   publishers => 
#   [
#     {
#        'type' => 'ArtifactArchiver',
#        'params' =>  {
#           "artifacts" => "**/*.tar.gz",
#        }
#     }
#    ],
#    assingedNode => 'linux'
# }

# jenkins_job::create{ 'opencv3.0-android' : 
#     axes => [
#     {
#       'type' => 'TextAxis', 
#       'params' => {
#         'name' => "TSYSTEM",
#         'values' => ['android']
#       }
#     },
#     {
#       'type' => 'TextAxis', 
#       'params' => {
#         'name' => "ARCH",
#         'values' => ['armeabi-v7a']
#       }
#     },   
#     {
#       'type' => 'TextAxis', 
#       'params' => {
#         'name' => "BTYPE",
#         'values' => ['release']
#       }
#     },    
#     {
#       'type' => 'TextAxis', 
#       'params' => {
#         'name' => "LTYPE",
#         'values' => ['shared']
#       }
#     }, 
#     {
#       'type' => 'TextAxis', 
#       'params' => {
#         'name' => "COMP",
#         'values' => ['gcc-4.9','clang-3.6']
#       }
#     },    
#   ], 
#   properties => 
#   [
#     { 
#       'type' => 'BuildDiscarderProperty',
#       'params' => { "numToKeep" => "2" }                
#     }
#   ],
#   triggers => 
#   [ 
#     {
#       'type' => "SCMTrigger",
#       'params' => {'spec' => 'H/60 * * * *'}
#     },
#   ],
#   scm => 
#   [
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'https://github.com/Itseez/opencv.git',
#         'branch' => '3.0.0',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'opencv'}
#             }
#         },
#     },
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'git@bitbucket.org:mschmieder/build-tools.git',
#         'branch' => 'master',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'build-tools'}
#             }
#         },
#     }
#   ],
#   builders => 
#   [
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# set -e

# mkdir -p $WORKSPACE/build
# mkdir -p $WORKSPACE/install/opencv

# cd $WORKSPACE/build
# x${WORKSPACE}/build-tools/scripts/bash/cmakew --source-directory $WORKSPACE/opencv --compiler ${COMP} --build-type ${BTYPE} --install-directory $WORKSPACE/install/opencv --target-system ${TSYSTEM} --target-architecture ${ARCH} --link-type ${LTYPE} --build --install --toolchain-directory ${HOME}/toolchains/

# cd $WORKSPACE/install
# tar -zcf $WORKSPACE/opencv3.0.0.tar.gz *
# '
#         },
#     },
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# rm -rf $WORKSPACE/build
# rm -rf $WORKSPACE/install/opencv
# '
#       },
#     }
#   ],
#   publishers => 
#   [
#     {
#        'type' => 'ArtifactArchiver',
#        'params' =>  {
#           "artifacts" => "**/*.tar.gz",
#        }
#     }
#    ],
# }

# jenkins_job::create{ 'opencv3.0-metrics' : 
#   properties => 
#   [
#     { 
#       'type' => 'BuildDiscarderProperty',
#       'params' => { "numToKeep" => "2" }                
#     }
#   ],
#   triggers => 
#   [ 
#     {
#       'type' => "SCMTrigger",
#       'params' => {'spec' => 'H/60 * * * *'}
#     },
#   ],
#   scm => 
#   [
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'https://github.com/Itseez/opencv.git',
#         'branch' => '3.0.0',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'opencv'}
#             }
#         },
#     },
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'git@bitbucket.org:mschmieder/build-tools.git',
#         'branch' => 'master',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'build-tools'}
#             }
#         },
#     }
#   ],
#   builders => 
#   [
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# set -e

# mkdir -p $WORKSPACE/build
# mkdir -p $WORKSPACE/install/opencv

# cd $WORKSPACE/build
# ${WORKSPACE}/build-tools/scripts/bash/cmakew --source-directory $WORKSPACE/opencv --compiler clang-3.6 --build-type release --target-system linux --link-type shared --build --analyze --analyze-output-directory $WORKSPACE/metrics
# '
#         },
#     },
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# rm -rf $WORKSPACE/build
# rm -rf $WORKSPACE/install/opencv
# '
#       },
#     }
#   ],
#   publishers => 
#   [
#     {
#       'type' => 'ClangScanBuildPublisher',
#       'params' => {
#         "reportFolderName" => "metrics",
#       }
#     }
#    ],
# }


# jenkins_job::create{ 'project_merge' : 
#   scm => 
#   [
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => 'git@bitbucket.org:mschmieder/build-tools',
#         'branch' => 'master',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'build-tools'}
#             }
#         },
#     }
#   ],
#   builders => 
#   [
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 'build-tools/scm-scripts/git_project_merge.sh --git-commit ${GIT_COMMIT} --git-url ${GIT_URL} --git-merge-branch ${GIT_MERGE_BRANCH}',
#       }
#     }
#   ]
# }

# jenkins_job::create{ 'TMPL_license_build' :
#   scm => 
#   [
#     {
#       'type' => "GitSCM",
#       'params' =>  {
#         'url' => '${LICENSE_GIT_URL}',
#         'branch' => '${LICENSE_GIT_COMMIT}',
#         'extensions' => {
#             'type' => 'RelativeTargetDirectory',
#             'params' => { 'relativeTargetDir' => 'license'}
#             }
#         },
#     },
#   ],
#   builders => 
#   [
#     {
#       'type' => "Shell",
#       'params' =>  {
#           'command' => 
# '
# cd license
# ./gradlew clean build
# ',
#       }
#     }
#   ],
#    publishers => 
#   [
#     {
#        'type' => 'ArtifactArchiver',
#        'params' =>  {
#           "artifacts" => "**/*.war",
#        }
#     }
#    ],
# }


# jenkins_job::create_ci{ 'license-master-checked' :
#   init_job_prefix => "INIT_", 
#   downstream_job_prefix => 'G_',
#   downstream_job_postfix => '_master', 
#   monitored_scms => 
#   [
#       {
#         url => 'git@bitbucket.org:mschmieder/license.git',
#         branch => 'master',
#         merge_branch => 'master',
#         directory => 'license'
#       },
#   ],
#   downstream_project_templates => ['TMPL_license_build'],
#   scm_trigger => 'H/60 * * * *' 
# }