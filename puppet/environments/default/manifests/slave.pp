Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }

file { '/home/vagrant/.ssh/id_rsa' :
  source => 'puppet:///data/id_rsa_jenkins',
  ensure => present,
  owner  => 'vagrant',
  group  => 'vagrant',
  mode   => '700'
}

################################################################################
# SETUP PACKAGING SYSTEM WITH APT
################################################################################
class { 'apt':
update => {
  frequency => 'daily',
  },
}

################################################################################
# INSTALL BASE SYSTEM
################################################################################
$sysPackages = [ 'wget', 'nano', 'pkg-config','openjdk-8-jre-headless']
package { $sysPackages:
  ensure => 'latest',
}

exec { 'java-cacerts':
  command => '/var/lib/dpkg/info/ca-certificates-java.postinst configure',
  require => Package['openjdk-8-jre-headless']
}

################################################################################
# GIT SETUP
################################################################################
class { 'git': }


git::config { 'user.name':
  value => 'jenkins-slave',
}

git::config { 'user.email':
  value => "jenkins-slave@$fqdn",
}

define pip3::package () {
  exec { "$name" :
    command      => "pip3 install $name",
    path         => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    require      => Package['python3-pip']
  }
}
  
class { 'jenkins::slave':
  masterurl => 'http://192.168.250.10:8080',
  version => '2.0',
  labels  => "linux"
}
exec { 'mkdir_jenkins_slave_ssh' :
  command => "mkdir -p /home/jenkins-slave/.ssh; chown -R jenkins-slave:jenkins-slave /home/jenkins-slave/.ssh",
  require => Class['jenkins::slave']
}
file { "/home/jenkins-slave/.ssh/id_rsa" :
  source => 'puppet:///data/id_rsa_jenkins',
  ensure => present,
  owner  => 'jenkins-slave',
  group  => 'jenkins-slave',
  mode   => '700',
  require => [Class['jenkins::slave'],Exec['mkdir_jenkins_slave_ssh']]
}
file { "/home/jenkins-slave/.ssh/config" :
  content => "StrictHostKeyChecking no",
  ensure => present,
  owner  => 'jenkins-slave',
  group  => 'jenkins-slave',
  mode   => '700',
  require => [Class['jenkins::slave'],Exec['mkdir_jenkins_slave_ssh']]
}

notice("JENKINS-NODE: running jenkins build configuration")
################################################################################
# BUILD ENVIRONMENT SETUP
################################################################################
$buildtools_packages = 
[ 'gcc',
  'gcc-4.9',
  'clang',
  'clang-3.6',
  'cmake',
  'python3-pip',
  'libtinyxml2-dev',
  'libjsoncpp-dev']
package { $buildtools_packages:
  ensure => 'latest',
}
class { 'gtest' : }
class { 'opencv' :
        dependencies_only => true,
        manage_packages => true 
      }
pip3::package{ 'pysftp' : }
pip3::package{ 'paramiko' : }
pip3::package{ 'beautifulsoup4' : }
