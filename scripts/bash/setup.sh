#!/usr/bin/env bash

#
# install the development environment
#
#

SCRIPT=`basename ${BASH_SOURCE[0]}`
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
WORKSPACE="${SCRIPT_DIR}/../../"
if [ `which realpath` ]; then
  WORKSPACE="$(realpath ${WORKSPACE})"
elif [ `which grealpath` ]; then
  WORKSPACE="$(grealpath ${WORKSPACE})"
fi

# find out on which system we are
BUILDSYSTEM_UNAME=$(uname)
BUILDSYSTEM_ARCHITECTURE=$(uname -m)
if [[ $BUILDSYSTEM_UNAME == *MINGW* ]] || [[ $BUILDSYSTEM_UNAME == *CYGWIN* ]]; then
  BUILDSYSTEM="windows"
  export PUPPET_EXECUTABLE="puppet.bat"
  export VAGRANT_EXECUTABLE="$(which vagrant)"
  export GIT_EXECUTABLE="$(which git)"
elif [[ $BUILDSYSTEM_UNAME == *Linux* ]]; then
  BUILDSYSTEM="linux"
  export PUPPET_EXECUTABLE="$(which puppet)"
  export VAGRANT_EXECUTABLE="$(which vagrant)"
  export GIT_EXECUTABLE="$(which git)"
else # darwin
  BUILDSYSTEM="macos"
  export PUPPET_EXECUTABLE="$(which puppet)"
  export VAGRANT_EXECUTABLE="$(which vagrant)"
  export GIT_EXECUTABLE="$(which git)"
fi

# setup puppet module variables
PUPPET_MODULE_CONF=${WORKSPACE}/puppet/puppet-module.conf
PUPPET_MODULE_INSTALL_DIR=${WORKSPACE}/puppet/modules

# install all puppet modules provided by puppet-module.conf
function install_puppet_modules() {
  while read line; do
    if [[ $line =~ ^# ]]; then
      # this is a comment line
      # echo "this is a comment line"
      echo -n ""
    else
      echo $line
      if [[ $line =~ ^puppet-forge ]]; then
        # puppet-forge puppetlabs-tomcat 1.4.1
        MODULE=$(echo $line | cut -d' ' -f2)
        VERSION=$(echo $line | cut -d' ' -f3)
        rm -rf ${PUPPET_MODULE_INSTALL_DIR}/${MODULE}
        if [[ -n "${VERSION}" ]]; then
          VERSION_ARG="--version ${VERSION}"
        fi
        # puppet module install puppetlabs-tomcat --version 1.4.1 --target-dir .
        ${PUPPET_EXECUTABLE} module install ${MODULE} ${VERSION_ARG} -i ${PUPPET_MODULE_INSTALL_DIR}
      fi

      if [[ $line =~ ^git ]]; then
        # git git@gitlab.cds.testo:puppet/chartdirector.git master
        URL=$(echo $line | cut -d' ' -f2)
        MODULE=$(echo $URL | sed 's/.git//g' | sed -E 's/ .*//g' | sed -E 's/.*\/([a-zA-Z0-9_-]+$)/\1/g' | sed -E 's/puppet-//g')
        HASH=$(echo $line | cut -d' ' -f3)
        PWD=$(pwd)

        cd ${PUPPET_MODULE_INSTALL_DIR}
        if [[ -d ${MODULE} ]]; then
         cd ${MODULE}
         ${GIT_EXECUTABLE} fetch origin
         if [[ -n "${HASH}" ]]; then
           ${GIT_EXECUTABLE} checkout ${HASH}
         fi
         ${GIT_EXECUTABLE} pull origin HEAD
        else
         ${GIT_EXECUTABLE} clone ${URL} ${MODULE}
         if [[ -n "${HASH}" ]]; then
           cd ${MODULE}
           ${GIT_EXECUTABLE} checkout ${HASH}
         fi
        fi

        cd ${PWD}
      fi
    fi
  done < ${PUPPET_MODULE_CONF}
}

# prepare/cehck system
function prepare_system() {

  echo "##########################################################################"
  echo "--> starting to setup puppet & vagrant modules..."
  echo "--> System Info  | SYSTEM:  ${BUILDSYSTEM}"
    echo "                 | ARCH:    ${BUILDSYSTEM_ARCHITECTURE}"
  if [ -z "${PUPPET_EXECUTABLE}" ];then
    echo "--> Puppet Info  | PATH:    <<missing>>"
    echo "                 | VERSION: <<unknown>>"
    IS_PUPPET_MISSING=true
  else
    echo "--> Puppet Info  | PATH:    ${PUPPET_EXECUTABLE} "
    echo "                 | VERSION: $(${PUPPET_EXECUTABLE} --version)"
  fi
  if [ -z "${VAGRANT_EXECUTABLE}" ];then
    echo "--> Vagrant Info | PATH:    <<missing>>"
    echo "                 | VERSION: <<unknown>>"
    IS_VAGRANT_MISSING=true
  else
    echo "--> Vagrant Info | PATH:    ${VAGRANT_EXECUTABLE} "
    echo "                 | VERSION: $(${VAGRANT_EXECUTABLE} --version)"
  fi
  if [ -z "${GIT_EXECUTABLE}" ];then
    echo "--> Git Info     | PATH:    <<missing>>"
    echo "                 | VERSION: <<unknown>>"
    IS_GIT_MISSING=true
  else
    echo "--> Git Info     | PATH:    ${GIT_EXECUTABLE} "
    echo "                 | VERSION: $(${GIT_EXECUTABLE} --version)"
  fi
  echo "##########################################################################"

  if [ -n "${IS_PUPPET_MISSING}" ];then
    echo "Error:        puppet not installed on host system"
    echo "              make sure you install puppet first before running setup.sh"
    echo "              see https://docs.puppetlabs.com/puppet/latest/reference/install_linux.html"
    echo "--> Installation:"
    echo "--> Ubuntu:"
    echo "        apt-get update -qq"
    echo "        apt-get install -y -q wget"
    echo '        wget --quiet https://apt.puppetlabs.com/puppetlabs-release-pc1-$(lsb_release -c | grep -Eo "(\w+)$").deb" -P /tmp/'
    echo "        dpkg -i /tmp/puppetlabs-release-pc1-$(lsb_release -c | grep -Eo "(\w+)$").deb"
    echo "        rm -f /tmp/${puppet_filename}"
    echo '        apt-get update -qq'
    echo '        apt-get install -y -q puppet-agent'
    echo '        echo "export PATH=$PATH:/opt/puppetlabs/bin" >> /etc/profile'

    exit 1
  fi

  if [ -n "${IS_VAGRANT_MISSING}" ];then
    echo "Error:        vagrant not installed on host system"
    echo "              make sure you install vagrant first before running setup.sh"
    echo "              see https://www.vagrantup.com"
    echo "--> Installation:"
    echo "--> Ubuntu:"
    echo "        apt-get update -qq"
    echo "        apt-get install -y -q vagrant"
    exit 1
  fi

  if [ -n "${IS_GIT_MISSING}" ];then
    echo "Error:        git not installed on host system"
    echo "              make sure you install git first before running setup.sh"
    echo "        apt-get update -qq"
    echo "        apt-get install -y -q git"
    exit 1
  fi


  IS_INSTALLED_VBGUEST=$(${VAGRANT_EXECUTABLE} plugin list | grep -o "vagrant-vbguest")
  if [ -z "${IS_INSTALLED_VBGUEST}" ];then
    ${VAGRANT_EXECUTABLE} plugin install vagrant-vbguest
  fi
}

##########################################################
#
prepare_system
install_puppet_modules
