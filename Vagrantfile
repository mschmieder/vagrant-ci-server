# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

current_dir    = File.dirname(File.expand_path(__FILE__))
vagrant_config = YAML.load_file("#{current_dir}/vagrant/config.yaml")

puppet_config  = vagrant_config['puppet']
boxes          = vagrant_config['boxes']
disk           = './secondDisk.vdi'

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  config.vm.define "server" do |server|
    opts=boxes['server']
    server.vm.box = opts['vagrant_box']
    server.vm.network opts['network_type'], ip: opts['ip']
    server.vm.hostname = opts['hostname']
    server.vm.synced_folder "./", "/vagrant", id: "vagrant-root"
    server.vm.synced_folder "./../", "/project", id: "vagrant-project"

    server.vm.provider "virtualbox" do |vb|
      server.vbguest.auto_update = true
      server.vbguest.no_remote = false
      vb.gui = false
      vb.memory = "1024"
    end
    server.vm.provision "shell", run: "always" do |s|
      s.path = "vagrant/bootstrap.sh"
    end
    server.vm.provision "shell", run: "always", inline: "ip addr show eth1 | grep 'inet '"

    # Enable shell provisioning to bootstrap puppet
    vec_manifests = opts['manifest_file']
    vec_manifests.each do |manifest|
      server.vm.provision :puppet do |puppet|
        puppet.environment       = puppet_config['environment']
        puppet.manifest_file     = manifest
        puppet.manifests_path    = "puppet/environments/"+puppet_config['environment']+"/manifests"
        puppet.environment_path  = "puppet/environments"
        puppet.module_path       = ["puppet/environments/"+puppet_config['environment']+"/modules","puppet/modules"]
        puppet.hiera_config_path = 'puppet/hiera/hiera.yaml'
    
        puppet.options = ["--fileserverconfig=/vagrant/puppet/fileserver.conf",puppet_config['options']]
      end
    end
  end



  config.vm.define "slave" do |slave|
    opts=boxes['slave']
    slave.vm.box = opts['vagrant_box']
    slave.vm.network opts['network_type'], ip: opts['ip']
    slave.vm.hostname = opts['hostname']
    slave.vm.synced_folder "./", "/vagrant", id: "vagrant-root"
    slave.vm.synced_folder "./../", "/project", id: "vagrant-project"

    slave.vm.provider "virtualbox" do |vb|
      slave.vbguest.auto_update = true
      slave.vbguest.no_remote = false
      vb.gui = false
      vb.memory = "1024"
    end
    slave.vm.provision "shell", run: "always" do |s|
      s.path = "vagrant/bootstrap.sh"
    end
    slave.vm.provision "shell", run: "always", inline: "ip addr show eth1 | grep 'inet '"

    # Enable shell provisioning to bootstrap puppet
    vec_manifests = opts['manifest_file']
    vec_manifests.each do |manifest|
      slave.vm.provision :puppet do |puppet|
        puppet.environment       = puppet_config['environment']
        puppet.manifest_file     = manifest
        puppet.manifests_path    = "puppet/environments/"+puppet_config['environment']+"/manifests"
        puppet.environment_path  = "puppet/environments"
        puppet.module_path       = ["puppet/environments/"+puppet_config['environment']+"/modules","puppet/modules"]
        puppet.hiera_config_path = 'puppet/hiera/hiera.yaml'
    
        puppet.options = ["--fileserverconfig=/vagrant/puppet/fileserver.conf",puppet_config['options']]
      end
    end
  end





end
